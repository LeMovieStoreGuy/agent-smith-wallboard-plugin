# Agent-Smith and Wallboard

Agent-Smith and Wallboard is a plugin for [Bamboo CI](https://www.atlassian.com/software/bamboo/).
Agent-Smith provides various statistics on the agents currently running on a Bamboo instance:

 - Agents currently running (or idling)
 - Usage of elastic agents (EC2 agents)
 - Current cost due to the elastic agents
 - Status of the build queue

## Access through the Wallboard
Those statistics are available through a Wallboard on

     http://BAMBOO_INSTANCE/CONTEXT_PATH/viewAgentWallboard.action

![Wallboard Screenshot](/atlassian/agent-smith-wallboard-plugin/raw/master/src/doc/wallboard.png)

### Customisation

Agent-Smith's behaviour can be modified with some parameters.

 - `secondsBeforeNextRefresh` allows to set the amount of time between each refresh of the screen (defaults to 15secs)
 - `minutesBeforeQueueItemOverdue` allows to set the amount of time after which a queued item is considered as overdue
   and is displayed in red.

### Sending data to other data service, e.g. graphite

Agent-Smith also uses the [Bamboo Graphite Connector](https://bitbucket.org/atlassian/bamboo-graphite-connector) to send the statistics of agents to a Graphite instance.
You need to install the Bamboo Graphite Connector plugin to do it.


