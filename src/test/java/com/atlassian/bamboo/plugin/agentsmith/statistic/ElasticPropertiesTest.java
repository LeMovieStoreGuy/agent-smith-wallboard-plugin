package com.atlassian.bamboo.plugin.agentsmith.statistic;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ElasticPropertiesTest {
    private Agent agent;

    @BeforeMethod
    public void setUp() throws Exception {
        agent = new Agent(0, Agent.Type.ELASTIC);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testPaymentTypeRequired() throws Exception {
        agent.getElasticProperties().setPaymentType(null);
    }
}
