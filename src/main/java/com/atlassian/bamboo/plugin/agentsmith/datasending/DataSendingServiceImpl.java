package com.atlassian.bamboo.plugin.agentsmith.datasending;

import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.plugin.monitoring.datasending.DataSender;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;
import org.jetbrains.annotations.NotNull;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static java.util.Map.Entry;

/**
 * Default implementation of the data sending service for agent Smith.
 * <p>
 * Automatically schedule an update with the {@link StatisticUpdateJob}.
 * </p>
 */
public class DataSendingServiceImpl implements DataSendingService, LifecycleAware, DisposableBean {
    private static final String PATH_AGENTS = "agents";
    private static final String PATH_ELASTIC = PATH_AGENTS + ".elastic";
    private static final String LABEL_ELASTIC_SPOT_PRICE = "us_east.spot_price";
    private static final String LABEL_ELASTIC_TOTAL_PENDING_TIME = "totalPendingTime";
    private static final String LABEL_ELASTIC_MAX_PENDING_TIME = "maxPendingTime";
    private static final String LABEL_DEDICATED_AGENT = "dedicated";
    private static final String PATH_QUEUE = "queue";
    private static final String LABEL_QUEUE_SIZE = "size";
    private static final String LABEL_QUEUE_MAX_TIME = "maxTime";
    private static final String LABEL_QUEUE_TOTAL_TIME = "totalTime";
    private static final String LABEL_AGENT_TOTAL = "total";
    private static final String LABEL_ELASTIC_HUNG_PENDING_AGENTS = "hungPending";
    private long interval = DEFAULT_REPORT_INTERVAL;
    /**
     * Default interval between each execution of {@link com.atlassian.bamboo.plugin.agentsmith.datasending.StatisticUpdateJob} in milliseconds.
     */
    private static final long DEFAULT_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30);

    private static final Logger logger = LoggerFactory.getLogger(DataSendingServiceImpl.class);
    private final PluginScheduler pluginScheduler;
    private final AgentSmithService agentSmithService;
    private final BundleContext bundleContext;


    /**
     * Creates a data sending service with its own scheduled update.
     *
     * @param pluginScheduler           scheduler for automatic updates.
     * @param agentSmithService         statistic gathering service.
     * @param bundleContext
     */
    public DataSendingServiceImpl(@NotNull final PluginScheduler pluginScheduler,
                                  @NotNull final AgentSmithService agentSmithService,
                                  @NotNull final BundleContext bundleContext) {
        this.pluginScheduler = pluginScheduler;
        this.agentSmithService = agentSmithService;
        this.bundleContext = bundleContext;
    }

    /**
     * Gets the name used by Graphite/Datadog to identify an agent type.
     *
     * @param agentType agent type to change into a String.
     * @return the string value of the agent type.
     */
    private static String getAgentTypeName(Agent.Type agentType) {
        switch (agentType) {
            case LOCAL:
                return "local";
            case REMOTE:
                return "remote";
            case ELASTIC:
                return "elastic";
            default:
                logger.error("Couldn't get the String version of Agent.Type: '{}'", agentType);
                return "unknown";
        }
    }

    /**
     * Gets the name used by Graphite/Datadog to identify an agent status.
     *
     * @param status agent status to change into a String.
     * @return the string value of the agent status.
     */
    private static String getStatusName(Status status) {
        switch (status) {
            case IDLE:
                return "idle";
            case BUSY:
                return "busy";
            case HUNG:
                return "hung";
            case DISABLED:
                return "disabled";
            case OFFLINE:
                return "offline";
            default:
                logger.error("Couldn't get the String version of Status: '{}'", status);
                return "unknown";
        }
    }

    /**
     * Gets the name used by Graphite/Datadog to identify an elastic agent status.
     *
     * @param elasticStatus elastic agent status to change into a String.
     * @return the string value of the elastic agent status.
     */
    private static String getElasticStatusName(ElasticProperties.ElasticStatus elasticStatus) {
        switch (elasticStatus) {
            case PENDING:
                return "pending";
            case PENDING_NO_INSTANCE:
                return "pendingNoInstance";
            case FAILED:
                return "failed";
            case SHUTTING_DOWN:
                return "shutting";
            case RUNNING:
                return "running";
            case OFFLINE:
                return "offline";
            case DISCONNECTED:
                return "disconnected";
            default:
                logger.error("Couldn't get the String version of ElasticProperties.ElasticStatus: '{}'", elasticStatus);
                return "unknown";
        }
    }

    /**
     * Gets the name used by Graphite/Datadog to identify an elastic agent payment type.
     *
     * @param instancePaymentType elastic agent payment type to change into a String.
     * @return the string value of the elastic agent payment type.
     */
    private static String getPaymentTypeName(InstancePaymentType instancePaymentType) {
        switch (instancePaymentType) {
            case REGULAR:
                return "regular";
            case SPOT:
                return "spot";
            case RESERVED:
                return "reserved";
            default:
                logger.error("Couldn't get the String version of InstancePaymentType: '{}'", instancePaymentType);
                return "unknown";
        }
    }

    @Override
    public void onStart() {
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(StatisticUpdateJob.DATA_SENDING_SERVICE_KEY, this);
        parameters.put(StatisticUpdateJob.AGENT_SMITH_SERVICE_KEY, agentSmithService);
        pluginScheduler.scheduleJob(StatisticUpdateJob.JOB_NAME, StatisticUpdateJob.class, parameters, new Date(), interval);
        logger.info("Reporting job has ben scheduled to run every {}ms, starting now.", interval);
    }

    @Override
    public void destroy() throws Exception {
        try {
            pluginScheduler.unscheduleJob(StatisticUpdateJob.JOB_NAME);
        } catch (IllegalArgumentException e) {
            logger.error("The job '{}' wasn't scheduled.", StatisticUpdateJob.JOB_NAME, e);
        }
    }

    @Override
    public void report(@NotNull final Statistics statistics) throws IOException
    {
        logger.debug("Sending the current metrics to Data Service");
        try (DataSender dataSender = new DataSender(bundleContext))
        {
            sendAgentsDetails(statistics, dataSender);
            sendElasticAgentsDetails(statistics, dataSender);
            sendQueueDetails(statistics, dataSender);
        }
        logger.debug("Successfully sent metrics to Data Service.");
    }

    private void sendAgentsDetails(final Statistics statistics, final DataSender dataSender) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        Map<Agent.Type, Statistics.Count<Status>> statusCountPerAgentType = statistics.getStatusCountPerAgentType();
        for (Entry<Agent.Type, Statistics.Count<Status>> typeEntry : statusCountPerAgentType.entrySet()) {
            String path = PATH_AGENTS + "." + getAgentTypeName(typeEntry.getKey());
            long totalAgents = 0L;
            for (Entry<Status, Long> statusEntry : typeEntry.getValue()) {
                long numberOfAgents = statusEntry.getValue();
                totalAgents += numberOfAgents;
                dataSender.send(path, getStatusName(statusEntry.getKey()), numberOfAgents, timestamp);
            }
            dataSender.send(path, LABEL_AGENT_TOTAL, (totalAgents), timestamp);
        }

        for(Entry<Agent.Type, Long> entry : statistics.getDedicatedAgentCount()) {
            String path = PATH_AGENTS + "." + getAgentTypeName(entry.getKey());
            dataSender.send(path, LABEL_DEDICATED_AGENT, entry.getValue(), timestamp);
        }
    }

    private void sendElasticAgentsDetails(@NotNull final Statistics statistics, @NotNull final DataSender dataSender) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        for (Entry<ElasticProperties.ElasticStatus, Long> elasticStatusEntry : statistics.getElasticStatusCount()) {
            dataSender.send(PATH_ELASTIC, getElasticStatusName(elasticStatusEntry.getKey()),
                    elasticStatusEntry.getValue(), timestamp);
        }

        Map<InstancePaymentType, Statistics.Count<Status>> statusCountPerPaymentType =
                statistics.getStatusCountPerPaymentType();
        for (Entry<InstancePaymentType, Statistics.Count<Status>> typeEntry : statusCountPerPaymentType.entrySet()) {
            String path = PATH_ELASTIC + "." + getPaymentTypeName(typeEntry.getKey());
            long totalAgents = 0L;
            for (Entry<Status, Long> statusEntry : typeEntry.getValue()) {
                long numberOfAgents = statusEntry.getValue();
                totalAgents += numberOfAgents;
                dataSender.send(path, getStatusName(statusEntry.getKey()), numberOfAgents, timestamp);
            }
            dataSender.send(path, LABEL_AGENT_TOTAL, totalAgents, timestamp);
        }
        dataSender.send(PATH_ELASTIC, LABEL_ELASTIC_TOTAL_PENDING_TIME,
                statistics.getElasticTotalWaitingTime(), timestamp);
        dataSender.send(PATH_ELASTIC, LABEL_ELASTIC_MAX_PENDING_TIME,
                statistics.getElasticMaxWaitingTime(), timestamp);
        dataSender.send(PATH_ELASTIC, LABEL_ELASTIC_SPOT_PRICE,
                statistics.getTotalCost(), timestamp);
        dataSender.send(PATH_ELASTIC, LABEL_ELASTIC_HUNG_PENDING_AGENTS,
                statistics.getNumHungPendingAgents(), timestamp);
    }

    private void sendQueueDetails(@NotNull final Statistics statistics, @NotNull final DataSender dataSender) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        List<QueuedJob> queuedJobs = statistics.getQueuedJobs();
        dataSender.send(PATH_QUEUE, LABEL_QUEUE_SIZE, Long.valueOf(queuedJobs.size()), timestamp);
        Date longestQueuedDate = statistics.getLongestQueuedDate();
        long maxTime = longestQueuedDate != null ? TimeUnit.MILLISECONDS.toSeconds(longestQueuedDate.getTime()) : 0;
        dataSender.send(PATH_QUEUE, LABEL_QUEUE_MAX_TIME, maxTime, timestamp);
        long totalQueueTime = statistics.getTotalQueueTime();
        dataSender.send(PATH_QUEUE, LABEL_QUEUE_TOTAL_TIME, totalQueueTime, timestamp);
    }

    /**
     * Gets the interval between each data update (in milliseconds).
     *
     * @return the interval between each data update.
     */
    public long getInterval() {
        return interval;
    }

    /**
     * Sets the interval between each data update (in milliseconds).
     *
     * @param interval interval between each data update (in milliseconds).
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    //@Override in platform3/bamboo 5.10 this method was incompatibly added to interface
    // comment out annotation to make it work on both 5.9.0 and 5.10.0
    public void onStop() {
        //TODO anything to do?
    }
}
