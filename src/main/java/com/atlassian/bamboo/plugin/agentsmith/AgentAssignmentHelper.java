package com.atlassian.bamboo.plugin.agentsmith;


import com.atlassian.bamboo.buildqueue.manager.AgentAssignmentService;
import com.atlassian.bamboo.buildqueue.manager.AgentAssignmentServiceHelper;
import com.atlassian.bamboo.spring.ComponentAccessor;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Supplier;

// Todo remove this once the helper method available in bamboo
public class AgentAssignmentHelper {
    private static final Supplier<AgentAssignmentService> agentAssignmentService = ComponentAccessor.AGENT_ASSIGNMENT_SERVICE;

    public static boolean isDedicatedAgent(BuildAgent buildAgent) {
        return !agentAssignmentService.get().getAgentAssignments().forExecutors(AgentAssignmentServiceHelper.asExecutors(buildAgent)).isEmpty();
    }
}
