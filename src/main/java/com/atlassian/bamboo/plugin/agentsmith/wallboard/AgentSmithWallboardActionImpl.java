package com.atlassian.bamboo.plugin.agentsmith.wallboard;

import com.atlassian.aws.ec2.RemoteEC2Instance;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstance;
import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.opensymphony.webwork.dispatcher.json.JSONArray;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type;

/**
 * Wallboard action in charge of converting the {@link Statistics} element into Json content.
 */
public class AgentSmithWallboardActionImpl extends AgentSmithWallboardAction {
    private static final Logger logger = LoggerFactory.getLogger(AgentSmithWallboardActionImpl.class);
    private static final String QUEUE_OVERDUE = "overdue";
    private static final String QUEUE_PLAN = "plan";
    private static final String QUEUE_KEY = "key";
    private static final String QUEUE_TIME = "time";
    private static final String QUEUE_STATUS = "status";
    private static final String QUEUE = "queue";
    private static final String EBS = "ebs";
    private static final String EC2 = "ec2";

    /**
     * Creates a Wallboard action.
     *
     * @param agentSmithService service in charge of generating statistics.
     */
    public AgentSmithWallboardActionImpl(AgentSmithService agentSmithService) {
        super(agentSmithService);
    }

    /**
     * Gets the name used to identify an agent type.
     *
     * @param agentType agent type to change into a String.
     * @return the string value of the agent type.
     */
    private static String getAgentTypeName(Type agentType) {
        switch (agentType) {
            case LOCAL:
                return "local";
            case REMOTE:
                return "remote";
            case ELASTIC:
                return "elastic";
            default:
                logger.error("Couldn't get the String version of '{}'", agentType);
                return "unknown";
        }
    }

    /**
     * Gets the name used to identify the agent availability for a queued job.
     *
     * @param agentAvailability agent availability to change into a String.
     * @return the string value of the agent availability.
     */
    private static String getAgentAvailabilityName(QueuedJob.AgentAvailability agentAvailability) {
        switch (agentAvailability) {
            case EXECUTABLE:
                return "waiting";
            case ELASTIC:
                return "error_elastic";
            case NONE:
                return "error";
            default:
                logger.error("Couldn't get the String version of '{}'", agentAvailability);
                return "unknown";
        }
    }

    @Override
    public JSONObject convertStatistics(Statistics statistics) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        addAgentsDetails(jsonObject, statistics);
        addQueueDetails(jsonObject, statistics);
        addElasticDetails(jsonObject, statistics);
        addElasticInstanceList(jsonObject, statistics);

        jsonObject.put("name", getBamboo().getAdministrationConfiguration().getInstanceName());

        return jsonObject;
    }

    private void addElasticInstanceList(JSONObject parentObject, Statistics statistics) throws JSONException {
        JSONArray elasticInstances = new JSONArray();

        for (Map.Entry<Long, Agent> agent: statistics.getAgents().entrySet()) {
            if (Type.ELASTIC == agent.getValue().getType()) {
                JSONObject elasticInstance = new JSONObject();
                ElasticProperties elasticProperties = agent.getValue().getElasticProperties();
                elasticInstance.put("agent_status", agent.getValue().getStatus());
                elasticInstance.put("ec2_status", elasticProperties.getElasticStatus().name());
                elasticInstance.put("name", elasticProperties.getElasticInstanceId());
                if(elasticProperties.getLaunchTime() == null)
                {
                    elasticInstance.put("uptime", -1l);
                }
                else
                {
                    elasticInstance.put("uptime", System.currentTimeMillis() - elasticProperties.getLaunchTime().getTime());
                }

                elasticInstances.put(elasticInstance);
            }
        }

        // send statistics of ec2 instances who are pending or whose agents are pending
        for(RemoteElasticInstance instance : statistics.getPendingElasticInstances())
        {
            JSONObject elasticInstance = new JSONObject();
            if(instance.isAgentLoading()) {
                elasticInstance.put("agent_status", "Pending");
                elasticInstance.put("ec2_status", "RUNNING");
            }
            else
            {
                elasticInstance.put("ec2_status", "Pending");
            }
            RemoteEC2Instance ei = instance.getInstance();
            elasticInstance.put("name", ei.getInstanceId());
            final Date launchTime = ei.getInstanceStatus().getLaunchTime();
            if (launchTime == null)
            {
                elasticInstance.put("uptime", -1L);
            }
            else
            {
                elasticInstance.put("uptime", System.currentTimeMillis() - launchTime.getTime());
            }

            elasticInstances.put(elasticInstance);
        }

        parentObject.put("elasticInstanceList", elasticInstances);
    }
    private void addAgentsDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        Map<Type, Statistics.Count<Status>> statusCountPerAgentType = statistics.getStatusCountPerAgentType();
        for (Map.Entry<Type, Statistics.Count<Status>> entry : statusCountPerAgentType.entrySet()) {
            String agentType = getAgentTypeName(entry.getKey());
            Statistics.Count<Status> count = entry.getValue();

            //The result is an array with in order busy, disabled, idle, hung
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(count.get(Status.BUSY));
            jsonArray.put(count.get(Status.DISABLED));
            jsonArray.put(count.get(Status.IDLE));
            jsonArray.put(count.get(Status.HUNG));

            parentObject.put(agentType, jsonArray);
        }
    }

    private void addQueueDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        JSONArray queue = new JSONArray();
        long currentTimestamp = System.currentTimeMillis();

        for (QueuedJob queuedJob : statistics.getQueuedJobs()) {
            JSONObject queueDetails = new JSONObject();

            queueDetails.put(QUEUE_OVERDUE, isOverdue(queuedJob));
            queueDetails.put(QUEUE_PLAN, queuedJob.getName());
            queueDetails.put(QUEUE_KEY, queuedJob.getKey());
            queueDetails.put(QUEUE_TIME, currentTimestamp - queuedJob.getQueuedTime().getTime());
            queueDetails.put(QUEUE_STATUS, getAgentAvailabilityName(queuedJob.getAgentAvailability()));

            queue.put(queueDetails);
        }
        parentObject.put(QUEUE, queue);
    }

    private void addElasticDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        //The result is an array with in order pending, shutting, failed, offline, totalCost
        JSONArray jsonArray = new JSONArray();

        Statistics.Count<ElasticProperties.ElasticStatus> count = statistics.getElasticStatusCount();
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.PENDING));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.SHUTTING_DOWN));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.FAILED));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.OFFLINE));

        jsonArray.put(statistics.getTotalCost());

        parentObject.put(EC2, jsonArray);
    }
}
