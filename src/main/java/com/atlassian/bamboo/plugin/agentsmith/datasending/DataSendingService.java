package com.atlassian.bamboo.plugin.agentsmith.datasending;

import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;

import java.io.IOException;

/**
 * Service in charge of sending agent Smith statistics to graphite and datadog.
 */
public interface DataSendingService
{
    /**
     * Send the current report to graphite and datadog.
     *
     * @param statistics statistics to send to graphite and datadog.
     */
    void report(Statistics statistics) throws IOException;
}
