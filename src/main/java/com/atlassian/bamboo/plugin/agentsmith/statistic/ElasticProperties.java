package com.atlassian.bamboo.plugin.agentsmith.statistic;

import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.aws.ec2.awssdk.AwsSupportConstants;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Date;

/**
 * Additional properties for an {@link Agent} with the {@link Agent.Type} {@link Agent.Type#ELASTIC}.
 */
public class ElasticProperties {

    private InstancePaymentType paymentType = InstancePaymentType.REGULAR;
    private ElasticStatus elasticStatus = ElasticStatus.UNKNOWN;
    private Date launchTime;
    private double cost;
    private String elasticInstanceId;

    /**
     * Creates a set of properties specific to elastic agents.
     */
    ElasticProperties() {
    /*
     * The reason of this declaration is to reduce the visibility of the constructor to prevent other classes to
     * make new instances.
     */
    }

    public InstancePaymentType getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the payment type.
     *
     * @param paymentType payment type, MUST NOT be null.
     * @throws IllegalArgumentException is the paymentType is null.
     */
    public void setPaymentType(InstancePaymentType paymentType) {
        if (paymentType == null) {
            throw new IllegalArgumentException("An elastic agent must have a payment type");
        }
        this.paymentType = paymentType;
    }

    public ElasticStatus getElasticStatus() {
        return elasticStatus;
    }

    public void setElasticStatus(ElasticStatus elasticStatus) {
        this.elasticStatus = elasticStatus;
    }

    public Date getLaunchTime() {
        return launchTime;
    }

    public void setLaunchTime(Date launchTime) {
        this.launchTime = launchTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getElasticInstanceId() {
        return elasticInstanceId;
    }

    public void setElasticInstanceId(String elasticInstanceId) {
        this.elasticInstanceId = elasticInstanceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ElasticProperties that = (ElasticProperties) o;

        if (Double.compare(that.cost, cost) != 0) return false;
        if (launchTime != null ? !launchTime.equals(that.launchTime) : that.launchTime != null) return false;

        return elasticStatus == that.elasticStatus && paymentType == that.paymentType;
    }

    @Override
    public int hashCode() {
        return 31 * paymentType.hashCode() + elasticStatus.hashCode();
    }

    @Override
    public String toString() {
        return "ElasticStatus: " + elasticStatus + ", PaymentType" + paymentType + ", "
                + "Cost: " + cost + ", LaunchTime: " + launchTime;
    }

    /**
     * Status of the elastic server (not agent).
     */
    public static enum ElasticStatus {
        /**
         * Started, waiting to be initialised.
         */
        PENDING,
        /**
         * Started without an bamboo instance yet.
         */
        PENDING_NO_INSTANCE,
        /**
         * Failed to start.
         */
        FAILED,
        /**
         * Currently shutting down.
         */
        SHUTTING_DOWN,
        /**
         * Taken offline.
         */
        OFFLINE,
        /**
         * Running.
         */
        RUNNING,
        /**
         * Disconnected
         */
        DISCONNECTED, 
        /**
         * https://extranet.atlassian.com/jira/browse/BUILDENG-7668
         */
        UNKNOWN
    }
}
